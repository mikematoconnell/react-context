import React from "react"
import { useClicks } from "./Context"

function ClicksShow() {
    const Clicks = useClicks()["clicks"]
    const increaseCount = useClicks()["updateClicks"]
    const resetCount = useClicks()["resetClicks"]
    // const [Clicks, increaseCount, resetCount] = useClicks()
    

    return (
        <>
            <p>{Clicks}</p>
            <button onClick={increaseCount}>Increase Count</button>
            <button onClick={resetCount}>Reset Count</button>
        </>
    )
}

export default ClicksShow