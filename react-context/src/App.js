import logo from './logo.svg';
import './App.css';
import { ClicksProvider } from './Context';
import Main from "./Main.js"
function App() {
  return (
    <ClicksProvider>
      <Main />
    </ClicksProvider>
  )
}

export default App;
