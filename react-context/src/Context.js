import { useContext, createContext, useState } from "react"

const ClicksContext = createContext()
const ClicksIncreaseContext = createContext()
const ClicksResetContext = createContext()

export function useClicks() {
    return useContext(ClicksContext)
}

export function useClicksIncrease() {
    return useContext(ClicksContext)
}

export function useClicksReset() {
    return useContext(ClicksContext)
}

export function ClicksProvider({ children }) {
    const [clicks, setClicks] = useState(0)

    function updateClicks() {
        setClicks(clicks + 1)
    }
    function resetClicks() {
        setClicks(0)
    }

    return (
        <ClicksContext.Provider value={{clicks, updateClicks, resetClicks}}>

                    { children }

        </ClicksContext.Provider>
    )
}